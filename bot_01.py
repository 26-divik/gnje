 ############################ DISCORD BOT ###################################### 
############################ by:- DIVIK GOEL ##################################
import discord
import random
from discord.utils import get
import functions as burn
import praw 
import time
import googletrans
import asyncio
import ast
from rps import gam
from functions import medit
from functions import udict
from langdetect import detect
from discord import Embed
from discord.utils import get
from discord.ext import commands
from PIL import Image 
from io import BytesIO

token = "token!"
bot = commands.Bot(command_prefix=commands.when_mentioned_or("gnje ","GNJE ","Gnje","Gnje ","gnje","GNJE","ganje","Ganje","GANJE","ganje ","Ganje ","GANJE ") , case_insensitive=True )
bot.remove_command('help')
reddit = praw.Reddit(client_id= "client_id",
    client_secret="client_secret",
    username= "username",
    password= "password",
    user_agent="user_agent")
lang = ('Afrikaans','af','Albanian','sq','Amharic','am','Arabic','ar','Armenian','hy','Azerbaijani','az','Basque','eu','Belarusian','be','Bengali','bn','Bosnian','bs','Bulgarian','bg','Catalan','ca','Cebuano','ceb','Chinese','zh','CN','Chinese2','TW','Corsican','co','Croatian','hr','Czech','cs','Danish','da','Dutch','nl','English','en','Esperanto','eo','Estonian','et','Finnish','fi','French','fr','Frisian','fy','Galician','gl','Georgian','ka','German','de','Greek','el','Gujarati','gu','Haitian Creole','ht','Hausa','ha','Hawaiian','haw','Hebrew','he','Hindi','hi','Hmong','hmn','Hungarian','hu','Icelandic','is','Igbo','ig','Indonesian','id','Irish','ga','Italian','it','Japanese','ja','Javanese','jv','Kannada','kn','Kazakh','kk','Khmer','km','Kinyarwanda','rw','Korean','ko','Kurdish','ku','Kyrgyz','ky','Lao','lo','Latin','la','Latvian','lv','Lithuanian','lt','Luxembourgish','lb','Macedonian','mk','Malagasy','mg','Malay','ms','Malayalam','ml','Maltese','mt','Maori','mi','Marathi','mr','Mongolian','mn','Myanmar' ,'Burmese','my','Nepali','ne','Norwegian','no','Nyanja','ny','Odia','or','Pashto','ps','Persian','fa','Polish','pl','Portuguese','pt','Punjabi','pa','Romanian','ro','Russian','ru','Samoan','sm','Scots Gaelic','gd','Serbian','sr','Sesotho','st','Shona','sn','Sindhi','sd','Sinhala','Sinhalese','si','Slovak','sk','Slovenian','sl','Somali','so','Spanish','es','Sundanese','su','Swahili','sw','Swedish','sv','Tagalog','Filipino','tl','Tajik','tg','Tamil','ta','Tatar','tt','Telugu','te','Thai','th','Turkish','tr','Turkmen','tk','Ukrainian','uk','Urdu','ur','Uyghur','ug','Uzbek','uz','Vietnamese','vi','Welsh','cy','Xhosa','xh','Yiddish','yi','Yoruba','yo','Zulu','zu','afrikaans','af','albanian','sq','amharic','am','arabic','ar','armenian','hy','azerbaijani','az','basque','eu','belarusian','be','bengali','bn','bosnian','bs','bulgarian','bg','catalan','ca','cebuano','ceb','chinese','zh','cn','chinese2','tw','corsican','co','croatian','hr','czech','cs','danish','da','dutch','nl','english','en','esperanto','eo','estonian','et','finnish','fi','french','fr','frisian','fy','galician','gl','georgian','ka','german','de','greek','el','gujarati','gu','haitian creole','ht','hausa','ha','hawaiian','haw','hebrew','he','hindi','hi','hmong','hmn','hungarian','hu','icelandic','is','igbo','ig','indonesian','id','irish','ga','italian','it','japanese','ja','javanese','jv','kannada','kn','kazakh','kk','khmer','km','kinyarwanda','rw','korean','ko','kurdish','ku','kyrgyz','ky','lao','lo','latin','la','latvian','lv','lithuanian','lt','luxembourgish','lb','macedonian','mk','malagasy','mg','malay','ms','malayalam','ml','maltese','mt','maori','mi','marathi','mr','mongolian','mn','myanmar' ,'burmese','my','nepali','ne','norwegian','no','nyanja','ny','odia','or','pashto','ps','persian','fa','polish','pl','portuguese','pt','punjabi','pa','romanian','ro','russian','ru','samoan','sm','scots gaelic','gd','serbian','sr','sesotho','st','shona','sn','sindhi','sd','sinhala','sinhalese','si','slovak','sk','slovenian','sl','somali','so','spanish','es','sundanese','su','swahili','sw','swedish','sv','tagalog','filipino','tl','tajik','tg','tamil','ta','tatar','tt','telugu','te','thai','th','turkish','tr','turkmen','tk','ukrainian','uk','urdu','ur','uyghur','ug','uzbek','uz','vietnamese','vi','welsh','cy','xhosa','xh','yiddish','yi','yoruba','yo','zulu','zu')
Role = "Member"
intents = discord.Intents.all()
intents.members = True


@bot.event
async def on_ready():
    print("Lets go !")
    await bot.change_presence(status=discord.Status.online, activity=discord.Game(name="minecraft"))
@bot.event
async def on_command_error(ctx,error):
  if isinstance(error,commands.errors.CommandNotFound):
    embed = Embed(title="I don't have that command",colour=burn.color())
    embed.add_field(name='Suggestion',value='To add this command \ntry to contact developers')
    await ctx.send(embed=embed)
  elif isinstance(error, commands.errors.MemberNotFound):
    embed = Embed(title="Member with that name was not found",colour=burn.color())
    embed.add_field(name='Suggestion',value='Mention a user to use that command')
    await ctx.send(embed=embed)
  elif isinstance(error, commands.errors.CommandOnCooldown):
    embed = Embed(title='Cooldown',description=f'You need to wait {error.retry_after} to use that command again',colour=burn.color())
    await ctx.send(embed=embed)
  else:
    embed = Embed(title='Error',colour=burn.color())
    embed.add_field(name='Traceback',value=error)
    await ctx.send(embed=embed)

@bot.command(name= 'help')
async def help(ctx,*,cat=None):
  if cat is None:
    embed = Embed(title="List of Commands Category",description="Displaying list of all commands",colour=burn.color())
    embed.add_field(name='Courtesy :raised_hands: ',value='gnje help Courtesy', inline=True)
    embed.add_field(name='Fun :joy:',value='gnje help fun',inline=True)
    embed.add_field(name='Image :camera:',value='gnje help img',inline=True)    
    embed.add_field(name='Games :game_die:',value='gnje help games',inline=True)  
    embed.add_field(name='Animals :frog:',value='gnje help animals',inline=True)    
    embed.add_field(name='Nerd stuff :nerd:',value='gnje help nerd',inline=True)    
    embed.add_field(name='NSFW :see_no_evil:',value='gnje help NSFW',inline=True)    
    embed.add_field(name='Other :shit:',value='gnje help other',inline=True)
    await ctx.send(embed=embed)
  elif str(cat).capitalize() == "Courtesy":
    embed = Embed(title="List of commands in Courtesy",description="Displaying list of all commands in courtesy",colour=burn.color())
    embed.add_field(name='Hi',value='Sends Hello! :pray:  ', inline=True)
    embed.add_field(name='Bye',value='gnje will reply you back :hand_splayed: ',inline=True)
    embed.add_field(name='Thanks', value='tag the user and Gnje will send thanks on your behalf :homes:',inline=True)
    await ctx.send(embed=embed)
  elif str(cat).capitalize() == "Fun":
    embed = Embed(title="List of commands in Fun",description="Displaying list of all commands in fun\ndon't forget to mention user",colour=burn.color())
    embed.add_field(name='Roast',value='gnje will roast him/her :fire:', inline=True)
    embed.add_field(name='Dick',value="""gnje will measure your dick ? :pinching_hand: """,inline=True)
    embed.add_field(name='Nudes', value="""Want some dhara's bobs & vagena :smiling_imp: """,inline=True)    
    embed.add_field(name='Meme', value="""Fresh memes with or without subgenre  :smiling_imp: """,inline=True) 
    await ctx.send(embed=embed)
  elif str(cat).capitalize() == "Img":
    embed = Embed(title="List of commands in Images",description="Displaying list of all commands in images",colour=burn.color())    
    embed.add_field(name='Fuck',value="just mention your friend :middle_finger:", inline=True)    
    await ctx.send(embed=embed)
  elif str(cat).capitalize() == "Games":
    embed = Embed(title="List of commands in Games",description="Displaying list of all commands in games",colour=burn.color())    
    embed.add_field(name='RPS',value="Plays Rock:rock: Paper:page_facing_up: Scissors:scissors:", inline=True)    
    await ctx.send(embed=embed)    
  elif str(cat).capitalize() == "Animals":
    embed = Embed(title="List of commands in Animals",description="Displaying list of all commands in Animals",colour=burn.color())    
    embed.add_field(name='woof',value="dog person? :dog:", inline=True)    
    embed.add_field(name='meow',value="cat person? :kissing_cat:", inline=True)
    await ctx.send(embed=embed)
  elif str(cat).capitalize() == "Nerd":    
    embed = Embed(title="List of commands in Nerd",description="Displaying list of all commands in nerd",colour=burn.color())    
    embed.add_field(name='translate',value="Translates given arg in given language :earth_asia:", inline=True)    
    embed.add_field(name='search',value="Search google for provided argument :mag:", inline=True)    
    embed.add_field(name='define',value="Searches Urban Dictionary for meaning of given word :interrobang:", inline=True)    
    await ctx.send(embed=embed)    
  elif str(cat).capitalize()== "Nsfw":
    embed = Embed(title="List of commands in NSFW",description="Displaying list of all commands in NSFW",colour=burn.color())    
    embed.add_field(name='bhabhi',value="bhabhi person? :kiss:", inline=True)    
    embed.add_field(name='bobs',value="bobs:tongue:", inline=True) 
    embed.add_field(name='darkjoke',value="No offense :japanese_ogre:", inline=True)
    await ctx.send(embed=embed)
  elif str(cat).capitalize() == "Other":
    embed = Embed(title="List of commands in Other",description="Displaying list of all commands in Other",colour=burn.color())
    embed.add_field(name='spam',value="gnje spam 'number >= 10' 'user' you can also add message if you want :repeat:", inline=True)
    embed.add_field(name='am i right ?',value="""stacy backs you up on an argument :100:""",inline=True)
    embed.add_field(name='?', value="""See if gnje is on or not :question:""",inline=True)    
    embed.add_field(name='Countdown', value="""gnje will countdown till 'number' :1234:""",inline=True) 
    await ctx.send(embed=embed)
  else:
    await ctx.send("Invalid Option")
    await help(ctx)

@bot.command(name='hi')
async def hi(ctx):
  await ctx.send(f"{burn.hi()} {ctx.message.author.mention}")
  
@bot.command(name='roast')
@commands.has_any_role('Roast', 'Owner','Admin')
async def roast(ctx,who:discord.Member):
  await ctx.send(f"{who.mention} {burn.burn()}")

@roast.error
async def roast_error(ctx,error):
  if isinstance(error, commands.errors.MissingRole):
    embed = embed("Missing Role")
    embed.add_field(name='Needed Role',value="Roast")
    await ctx.send(embed=embed)

@bot.command(name='dick')
async def dick(ctx,user : discord.Member):
 if medit(user.edit).get ('Member id') == #your Member id :
    if ctx.author.id ==#your Member id :
      bed = Embed(title=f"We know you have a long dick! you don't have to brag about it!",color=burn.color())
      await ctx.send(embed=bed)
    else:
      await ctx.send(f"{user.mention} go and ask from your mom")
 elif medit(user.edit).get('Member id') == #bot's Member id :
    if ctx.author.id == #your Member id :
      await ctx.send("You testing again ?")
    else:
      bed = Embed(title=f"I don't have one",color=burn.color())
      await ctx.send(embed=bed)
 else:
    bed = Embed(title=f"MEASURING PP ",description=f"{user.mention}'s PP\n8{burn.dick()}D",color=burn.color())
    await ctx.send(embed=bed)

@bot.command(name = 'bye')
async def bye( ctx):
  await ctx.send(ctx.message.author.mention +" Sayonara")

@bot.command(name = 'thanks')
async def thanks( ctx,user : discord.Member):
  await ctx.send("Arigato " + user.mention)

@bot.command(name='right?', aliases=['am','right','correct?'])
async def am(ctx,*,questiom):
  await ctx.send(f"{burn.agree()}")

@bot.command(name='?')
async def name(ctx):
    await ctx.send("Yes?")

@bot.command(name='spam')
async def spam(ctx,num,*who):
  g = ''
  t = 0
  for r in who :
    g += f"{r} "
    t += 1
  who = g 
  if int(num) == 0:
    await ctx.send("Get a life  " + ctx.message.author.mention)  
  elif int(num) <= 10 :
    for i in range (int(num)):
      if t > 1 :
        await ctx.send(f"{who}\n")
      else :
        await ctx.send(f"You have been spammed {who}\n")
  else : 
    await ctx.send("Get a life  " + ctx.message.author.mention)

@bot.command(name='meme')
async def meme(ctx,subred = 'memes'):
  subreddit = reddit.subreddit(subred)           
  all_subs=[]

  top = subreddit.top(limit=50)

  for submission in top:
    all_subs.append(submission)

  random_sub= random.choice(all_subs)

  name = random_sub.title
  url= random_sub.url

  em = discord.Embed(title=name,colour=burn.color())

  em.set_image(url=url)
  await ctx.send(embed =em)

@bot.command(name="nudes")
async def nudes(ctx):
  message = await ctx.send("calling mia")
  await message.edit(content="calling mia..")
  await message.edit(content="calling mia...")
  await message.edit(content="mia please send your nudes") 
  await message.edit(content="mia please send your nudes") 
  await message.edit(content='mia - "check your discord"') 
  await message.edit(content='mia - "check your discord"') 
  await message.edit(content="sending.")
  await message.edit(content="sending...")
  await message.edit(content=" :middle_finger: ")
  await ctx.send(file=discord.File('hide.jpg'))

@bot.command(name='countdown')
async def countdown(ctx, secs:int):
  if secs <= 10:
    for x in range(0,secs):
      await ctx.send(x)
      await asyncio.sleep(1)
  elif secs <= 60 and secs >=11:
    cal = (secs/100) * 10
    t = 0
    for x in range(0,11):
      await ctx.send(t)
      t = t + int(cal)
      await asyncio.sleep(cal)
  else:
    await ctx.send("I ain't free!")

@bot.command(name='woof')
async def woof(ctx,subred = 'dogpictures'):
  subreddit = reddit.subreddit(subred)           
  all_subs=[]

  top = subreddit.top(limit=100)

  for submission in top:
    all_subs.append(submission)

  random_sub= random.choice(all_subs)
  if not random_sub.is_self:  
    url = random_sub.url
  name = random_sub.title

  em = discord.Embed(title=name,colour=burn.color())

  em.set_image(url=url)
  await ctx.send(embed =em)

@bot.command(name='meow')
async def meow(ctx,subred = 'catpictures'):
  subreddit = reddit.subreddit(subred)           
  all_subs=[]

  top = subreddit.top(limit=50)

  for submission in top:
    all_subs.append(submission)

  random_sub= random.choice(all_subs)

  name = random_sub.title
  url= random_sub.url

  em = discord.Embed(title=name,colour=burn.color())

  em.set_image(url=url)
  await ctx.send(embed =em)

@bot.command(name='bhabhi')
async def bhabhi(ctx,subred = 'SoSaree'):
  if ctx.channel.is_nsfw() :
    subreddit = reddit.subreddit(subred)           
    all_subs=[]

    top = subreddit.top(limit=50)

    for submission in top:
      all_subs.append(submission)

    random_sub= random.choice(all_subs)

    name = random_sub.title
    url= random_sub.url

    em = discord.Embed(title=name,colour=burn.color())

    em.set_image(url=url)
    await ctx.send(embed =em)
  else:
    await ctx.send("You need to use this command in a NSFW channel!")

@bot.command(name='fuck')
async def fuck(ctx,user:discord.Member):
  fuck = Image.open('fuck.jpg')
  asset = user.avatar_url_as(size=4096)
  data = BytesIO(await asset.read())
  pfp = Image.open(data)
  pfp = pfp.resize((120,120))
  fuck.paste(pfp,(366,147))
  fuck.save("profile.png")
  await ctx.send(file= discord.File("profile.png")) 

@bot.command(name='translate')
async def translate(ctx,*,text):
  if text.endswith(lang):
    spl_string = text.split()
    rm = spl_string[:-2]
    listToStr = ' '.join([str(elem) for elem in rm])
    embed = Embed(title=f"Translation for {text}",colour=burn.color())
    embed.add_field(name='Translated text:',value=((googletrans.Translator().translate(listToStr,dest=spl_string[-1])).text), inline= False)
    embed.add_field(name='Pronunciation:',value=((googletrans.Translator().translate(listToStr,dest=spl_string[-1])).pronunciation), inline=False)
    embed.add_field(name='Translated in:',value=burn.get_key(((googletrans.Translator().translate(listToStr,dest=spl_string[-1])).dest)), inline=True)
    embed.add_field(name='Source language:',value=burn.get_key(((googletrans.Translator().translate(listToStr,dest=spl_string[-1])).src)), inline=True)
    await ctx.send(embed=embed)
  else : 
    embed = Embed(title= "Something went wrong",value="Please enter vallid language",colour=burn.color())
    await ctx.send(embed=embed)

@bot.command(name='define')
async def define(ctx, *what):
  word = ''
  for x in what:
    word += f'{x} '
  reply = udict(word)
  embd = Embed(title=f"{word}",colour=burn.color())
  embd.add_field(name="Meaning",value=f"{reply}",inline=True)
  await ctx.send(embed=embd)

@bot.command(name='search')
async def search(ctx, *what):
    string = ""
    for i in what:
        string += f'{i} '
    string.rstrip()
    ans = burn.srch(string)
    await ctx.send(ans)

@bot.command(name='bobs')
async def bobs(ctx,subred = 'boobs'):
  if ctx.channel.is_nsfw() :
    subreddit = reddit. subreddit(subred)  
    all_subs=[]

    top = subreddit.top(limit=50)

    for submission in top:
      all_subs.append(submission)

    random_sub= random.choice(all_subs)

    name = random_sub.title
    url= random_sub.url

    em = discord.Embed(title=name,colour=burn.color())

    em.set_image(url=url)
    await ctx.send(embed =em)
  else:
    await ctx.send("You need to use this command in a NSFW channel!")

@bot.command(name="stars")
async def stars(ctx,num,inp:int):
  if num == "1" :
    if int(inp) <= 10: 
      for i in range(inp):
        k = "* "*(i+1)
        await ctx.send(f"`{k}`")
    else :
      await ctx.send ("wanna see my mp5.?")
  elif num == "2" :
    if int(inp) <= 10: 
      for i in range(inp,0,-1):
        k = "* "*((i+1)-1)    
        await ctx.send(f"`{k}`")
    else :
      await ctx.send ("wanna see my mp5.?")
  elif num == "3" :
    if int(inp) <= 10: 
      for i in range(inp):
          k = 2*(" " * (inp-(i+1)))
          l = " *" * (2*i+1)
          await ctx.send(f"```{k}{l}{k}```") 
    else :
      await ctx.send ("wanna see my mp5.?")
  else :
    pass

@bot.command(name='darkjoke')
async def darkjoke(ctx,subred = 'darkjokes'):
  if ctx.channel.is_nsfw() :
    subreddit = reddit. subreddit(subred)  
    all_subs=[]

    top = subreddit.top(limit=100)

    for submission in top:
      all_subs.append(submission)

    random_sub= random.choice(all_subs)

    name = random_sub.title
    description= random_sub.selftext
    await ctx.send(f"{name}\n{description}")
  else:
    await ctx.send("You need to use this command in a NSFW channel!")

@bot.command(name='darkmeme')
async def darkmeme(ctx,subred = 'darkmemers'or'dankmemes'):
  if ctx.channel.is_nsfw() :
    subreddit = reddit. subreddit(subred)  
    all_subs=[]

    top = subreddit.top(limit=100)

    for submission in top:
      all_subs.append(submission)

    random_sub= random.choice(all_subs)

    name = random_sub.title
    url= random_sub.url

    em = discord.Embed(title=name,colour=burn.color())

    em.set_image(url=url)
    await ctx.send(embed =em)
  else:
    await ctx.send("You need to use this command in a NSFW channel!")

@bot.command(name='say_sorry_to')
async def say_sorry(ctx, who:discord.Member):
    def check(m):
        return m.author.id == ctx.author.id

    if ctx.author.id == #your member's id: 
        await ctx.send('but...')
        try:
            say = await bot.wait_for('message', check=check, timeout=10)
        except asyncio.TimeoutError:
            await ctx.send(f"Aight {who.mention} I'm Sorry")
        if str(say).capitalize() == "Say":
            await ctx.send(f"Okay, hey {who.mention} I'm Sorry")
        else:
            await ctx.send(f"Okay, hey {who.mention} I'm Sorry")
    else:
        await ctx.send(f"Fuck Off")

@bot.command(name='rps')
async def rsp(ctx):
    def check(m):
        return m.author.id == ctx.author.id
    
    Gnje = 0
    player = 0
    while True :
        x = []
        await ctx.send("Rock, Paper or Scissors")
        try:
            playerChoice = await bot.wait_for('message', check=check, timeout=30)
        except asyncio.TimeoutError:
            await ctx.send(f"<@!{ctx.author.id}> didn't answer in time, what a noob.")
            break;

        a = gam(playerChoice.content)
        for c in a:
            x.append(c)

        await ctx.send(x[1])

        if x[0] == "You Win":
            await ctx.send("I lost.!")
            await ctx.send(" Another One?")
            await ctx.send("just enter 'YES/NO'")
            player += 1
            try:
                ao = await bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Timed Out")
                break;

            if str(ao.content).capitalize() == "No":
                 break;

        elif x[0] == "You Lose":
            await ctx.send("I Won.!")
            await ctx.send("Another One?")
            await ctx.send("just enter 'YES/NO'")
            Gnje += 1
            try:
                ao = await bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Timed Out")
                break;

            if str(ao.content).capitalize() == "No":
                break;
            
        elif x[0] == "Tie":
            await ctx.send("Tie...huh!")
            await ctx.send("Let's play Another One?")
            await ctx.send("just enter 'YES/NO'")
            
            try:
                ao = await bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Timed Out")
                break;

            if str(ao.content).capitalize() == "No":
                break;
            
            
        else:
            await ctx.send(a)

    await ctx.send(f'Final Scores Gnje: {Gnje} and <@!{ctx.author.id}> : {player}')

@bot.command(name="what")
async def what(ctx,*,message):
  await ctx.send("I just shat all over the place")

@bot.command()
async def ping(ctx):
    await ctx.send(f'Pong! {round(bot.latency*1000, 1)} ms')

@bot.command(name='mute')
async def mute(ctx,who:discord.Member=None):
    if who is None:
        await ctx.send("Mention someone to mute")
        return
    Role = "MUTED"
    role = get(ctx.guild.roles, name=Role)
    await who.add_roles(role)
    await ctx.send('https://i.pinimg.com/736x/81/da/f8/81daf8311f01426b311249df11f36a33.jpg')

@bot.command(name='unmute')
async def unmute(ctx,who:discord.Member=None):
    if who is None:
        await ctx.send("Mention someone to mute")
        return
    Role = "MUTED"
    role = get(ctx.guild.roles, name=Role)
    await who.remove_roles(role)
    await ctx.send(f'Unmuted {who.mention}')

bot.run(token)
