#!/bin/python3.8
import random

li = ['R','P','S']

def res(inp,cip):
    return f"Your choice:{inp}\nComputer choice:{cip}\nResult:"

def gam(inp):
    uinp = str(inp).capitalize()
    k = uinp
    uinp = uinp[0]
    cop = random.choice(li)

    if uinp == cop:
        return (f'Tie'),k
    elif uinp == "R" and cop == "P":
        return (f'You Lose'),'Paper'
    elif uinp == "R" and cop == "S":
        return (f'You Win'),'Scissors'
    elif uinp == "P" and cop == "S":
        return (f'You Lose'),'Scissors'
    elif uinp == "P" and cop == "R":
        return (f'You Win'),'Rock'
    elif uinp == "S" and cop == "R":
        return (f'You Lose'),'Rock'
    elif uinp == "S" and cop == "P":
        return (f'You Win'),'Paper'
    else:
        return ('Enter Rock, Paper or Scissor','Not a valid option')